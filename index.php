<html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ma galerie</title>
	<style>
		li {display : inline;}
	</style>
	<link href="dist/css/lightbox.css" rel="stylesheet" />
	<link rel="stylesheet" href="style.css">
</head>

<body>

	<h1>Galerie Halloween !</h1>
	<!-- <pre>La fonction pathinfo ($nomFichier) renvoie
	- le chemin, 
	- le nom de fichier avec extension 
	- l'extension 
	- et le nom de fichier sans extension </pre> -->

	<ul>
		<li>
			<?php 
				$arFichiers = glob('mini/*');
				foreach($arFichiers as $fic) {
					//Parsing : Récupération des champs du nom de fichier
					$arFic = pathinfo($fic);
					echo '<li><a href="pics'."/".$arFic['basename'].'" alt="'.$arFic['filename'].'" data-lightbox="galerie">'.
					'<img src="'.$fic.'" loading = "lazy" alt="'.$arFic['filename'].'"></a></li>'."\n\t\t";
				}
			?>

		</li>
	</ul>


	<script src="dist/js/lightbox-plus-jquery.js"></script>

</body>
</html>